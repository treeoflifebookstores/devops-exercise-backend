import configparser
from flask import Flask, request
from flask_cors import CORS
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
import requests
import uuid

config_parse = configparser.ConfigParser()
config_parse.read('.env')

try:
    webhook_url = config_parse['DEFAULT']['SLACK_KEY']
    db_url = config_parse['DEFAULT']['DATABASE_URI']
except KeyError:
    print("SLACK_KEY or DATABASE_URI not found. Check '.env' file.")


app = Flask(__name__)
cors = CORS(app, resources={r"/api/*": {"origins": "*"}})
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SQLALCHEMY_DATABASE_URI'] = db_url
db = SQLAlchemy(app)


class Message(db.Model):
    __tablename__ = "messages"

    id = db.Column(db.String, primary_key=True, index=True)
    message = db.Column(db.String)
    response = db.Column(db.String)
    timestamp = db.Column(db.DateTime(timezone=True),
                          server_default=db.func.now())


try:
    migrate = Migrate(app, db, compare_type=True)
except Exception as em:
    print(em)


@app.route("/")
def root():
    return {"message": "MAIN PAGE"}


@app.route("/api/message")
def message():
    message = request.args.get('message')
    response = send_to_slack(message)
    message_id = str(uuid.uuid4())
    message_data = Message(id=message_id, message=message, response=response)
    db.session.add(message_data)
    db.session.commit()
    response_data = {"id": message_id,
                     "message": message, "response": response}
    return response_data


@app.route("/api/messages")
def get_messages():
    messages = [u.__dict__ for u in Message.query.limit(limit=100).all()]
    for row in messages:
        row.pop('_sa_instance_state', None)
    results_dict = {'messages': messages}
    return results_dict


def send_to_slack(message: str):
    post = {'username': 'Dev Exercise',
            'icon_emoji': ':robot_face:',
            'channel': '#devops-exercise-input',
            'text': message
            }
    try:
        url = f"https://hooks.slack.com/services/{webhook_url}"
        headers = {'Content-Type': 'application/json'}
        slack_req = requests.post(url, headers=headers, json=post)
        return slack_req.text
    except Exception as em:
        print("EXCEPTION: " + str(em))
        return str(em)


if __name__ == '__main__':
    app.run()
