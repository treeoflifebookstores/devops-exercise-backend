# DevOps Exercise Backend

## Primary Package Docs
- [Flask](https://flask.palletsprojects.com/en/1.1.x/)
- [Flask SQLAlchemy](https://flask-sqlalchemy.palletsprojects.com/en/2.x/)

## Local Systems Prerequisites
- [Install Pyenv](https://github.com/pyenv/pyenv)
- [Install Pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv)

---

## Setup
```
# Replace pyenv with virtualenv pattern if preferred
python --version
pyenv install 3.10.4 -s
pyenv virtualenv 3.10.4 devops-exercise-backend
pyenv activate devops-exercise-backend
pyenv version
python --version

pip install -r requirements.txt
cp .env.template .env
# Update 'SLACK_KEY' in .env file with provided key
```

### Notes:
- When setting values in the `.env`, do not use quotes (single or double) with the value
	- GOOD: `SLACK_KEY=ASDFALSKDJFASDF`
	- BAD: `SLACK_KEY="ASDFALSKDJFASDF"`

## Run Locally
```
# DB IN DOCKER (If you have it - not required to use docker, run what is comfortable)
docker stop devops-test-db
docker run --name devops-test-db -p 5432:5432 -e POSTGRES_PASSWORD=PASSWORD --rm -d postgres:15.2

# PYTHON VIA PYENV
export FLASK_DEBUG=1
export FLASK_APP=app.py
flask db upgrade
flask run

# TROUBLESHOOTING DB SETUP/INTIALIZATION
### After migrating 'Flask-SQLAlchemy' and 'Flask-Migrate', had to rebuild DB migrations:
rm -rf migrations
flask db init
flask db migrate
```

## Simple test once running
In a browser tab, copy/paste the following:
```
# SEND SLACK MESSAGE + STORE IN DB - Returns single JSON object
http://localhost:5000/api/message?message=Hello%20from%20the%20backend
# LIST ALL MESSAGES STORED IN DB - Returns a list of objects based on rows in the DB
http://localhost:5000/api/messages
```
